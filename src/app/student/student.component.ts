import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.geStudents();
  }

  geStudents() {
    this.http.get('http://localhost:8080/SpringRestHibernate/students', {responseType: 'text'})
      .subscribe(data => {
        console.log("--------------->");
        console.log(data)
      });
  }


}
